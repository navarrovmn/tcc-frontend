import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Session/Login'
import Sign from '@/components/Session/Sign'
import store from '@/store'

Vue.use(Router)

export const router = new Router({
  routes: [
    { path: '/', name: 'HelloWorld', component: HelloWorld },
    { path: '/login', name: 'Login', component: Login },
    { path: '/sign', name: 'Sign', component: Sign},
    { path: '*', redirect: '/' }
  ]
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/', '/login', '/sign']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = Object.keys(store.state.user).length === 0

  if(authRequired && !loggedIn) return next('/login')

  next()
})

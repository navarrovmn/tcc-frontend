import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import { mutations } from './mutations'
import * as actions from './actions'

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: 'state',
  storage: localStorage
})

const state = {
    user: {}
}

export default new Vuex.Store({
    state,
    mutations,
    actions,
    plugins: [vuexPersist.plugin]
})

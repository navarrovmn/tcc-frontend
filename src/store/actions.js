import * as types from './mutation-types'

const apiUrl = 'http://localhost:3000'

export const signIn = ({ commit }, user_payload) => {
    commit(types.SIGN_IN, user_payload)
}

export const login = ({ commit }) => {
  commit(types.LOGIN)
}

// export const signOut = ({ commit }) => {
//     commit(types.SIGN_OUT)
// }
